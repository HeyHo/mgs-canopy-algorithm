#!/usr/bin/env python3

import unittest
from pprint import pformat
from urllib import request
from os import path, mkdir
import gzip
from datetime import datetime
from subprocess import Popen, PIPE

import logging
logger = logging.getLogger("canopy_test")
logger.setLevel(logging.DEBUG)

log_file_handler = logging.FileHandler("canopy_test.log")
log_file_handler.setLevel(logging.DEBUG)
log_stream_handler = logging.StreamHandler()
log_stream_handler.setLevel(logging.DEBUG)
logger.addHandler(log_file_handler)
logger.addHandler(log_stream_handler)
info = logger.info
debug = logger.debug
warn = logger.warning
err = logger.error


class CanopyClusteringEndEndTest(unittest.TestCase):

    canopy_bin_path = None
    canopy_bin_possible_paths = ["./canopy", "../canopy", "../src/canopy"]
    demo_abundance_matrix_url = "https://www.cbs.dtu.dk/projects/CAG/demo_abundance_matrix.txt.gz"

    def setUp(self):
        if not path.isfile("demo_abundance_matrix.txt"):
            info("Demo abundance matrix not found. Downloading.")
            request.urlretrieve(self.demo_abundance_matrix_url, "demo_abundance_matrix.txt.gz")
            info("Demo abundance matrix retrieved.")
            with gzip.open("demo_abundance_matrix.txt.gz", "rb") as f_gz:
                with open("demo_abundance_matrix.txt", "wb") as f:
                    f.write(f_gz.read())
            info("Demo abundance matrix unzipped.")
        else:
            info("Demo abundance matrix found.")

        for canopy_bin_possible_path in self.canopy_bin_possible_paths:
            if path.isfile(canopy_bin_possible_path):
                self.canopy_bin_path = canopy_bin_possible_path
                break
        if self.canopy_bin_path is None:
            err("Can't find canopy. I looked in: {}\nPlease make sure compilation ended successfully.".format(pformat(self.canopy_bin_possible_paths)))
            exit(1)
        else:
            info("I'm using canopy found in: {}".format(self.canopy_bin_path))

    def test_run_default(self):
        run_dir_name = "./test_run_{}/".format(str(datetime.now()).replace(" ", "_"))
        mkdir(run_dir_name)

        canopy_command = '../{} -n 4 -i ../demo_abundance_matrix.txt -o demo.out -c demo_profiles.out -p demo ' \
                     '--max_canopy_dist 0.1 --max_merge_dist 0.1'.format(self.canopy_bin_path)

        with Popen(canopy_command, shell=True, stdout=PIPE, stderr=PIPE, bufsize=10, cwd=run_dir_name) as canopy_process:
            for line in canopy_process.stdout:
                info(">>\t" + line.decode('utf-8').rstrip())
            for line in canopy_process.stderr:
                warn(">>\t" + line.decode('utf-8').rstrip())

        self.assertEqual(canopy_process.returncode, 0, "Canopy Clustering process finished with error (non-zero return code)")

        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output file not created")
        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output cluster file not crated")

        with open(path.join(run_dir_name, "demo_profiles.out"), "r") as output_profiles_f:
            self.assertEqual(len(output_profiles_f.readlines()), 19, "Output number of clusters on demo matrix is different than 19")

    def test_run_default_with_zipped_file(self):
        run_dir_name = "./test_run_{}/".format(str(datetime.now()).replace(" ", "_"))
        mkdir(run_dir_name)

        gzip_command = 'gzip -c ../demo_abundance_matrix.txt > ../demo_abundance_matrix.gz'
        with Popen(gzip_command, shell=True, stdout=PIPE, stderr=PIPE, bufsize=10, cwd=run_dir_name) as gzip_process:
            for line in gzip_process.stdout:
                info(">>\t" + line.decode('utf-8').rstrip())
            for line in gzip_process.stderr:
                warn(">>\t" + line.decode('utf-8').rstrip())
        self.assertEqual(gzip_process.returncode, 0)

        canopy_command = '../{} -n 4 -i ../demo_abundance_matrix.gz -o demo.out -c demo_profiles.out -p demo ' \
                     '--max_canopy_dist 0.1 --max_merge_dist 0.1'.format(self.canopy_bin_path)

        with Popen(canopy_command, shell=True, stdout=PIPE, stderr=PIPE, bufsize=10, cwd=run_dir_name) as canopy_process:
            for line in canopy_process.stdout:
                info(">>\t" + line.decode('utf-8').rstrip())
            for line in canopy_process.stderr:
                warn(">>\t" + line.decode('utf-8').rstrip())

        self.assertEqual(canopy_process.returncode, 0, "Canopy Clustering process finished with error (non-zero return code)")

        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output file not created")
        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output cluster file not crated")

        with open(path.join(run_dir_name, "demo_profiles.out"), "r") as output_profiles_f:
            self.assertEqual(len(output_profiles_f.readlines()), 19, "Output number of clusters on demo matrix is different than 19")

    def test_run_mmap(self):
        run_dir_name = "./test_run_{}/".format(str(datetime.now()).replace(" ", "_"))
        mkdir(run_dir_name)

        canopy_command = '../{} --dont_use_mmap -n 4 -i ../demo_abundance_matrix.txt -o demo.out -c demo_profiles.out -p demo ' \
                     '--max_canopy_dist 0.1 --max_merge_dist 0.1'.format(self.canopy_bin_path)

        with Popen(canopy_command, shell=True, stdout=PIPE, stderr=PIPE, bufsize=10, cwd=run_dir_name) as canopy_process:
            for line in canopy_process.stdout:
                info(">>\t" + line.decode('utf-8').rstrip())
            for line in canopy_process.stderr:
                warn(">>\t" + line.decode('utf-8').rstrip())

        self.assertEqual(canopy_process.returncode, 0, "Canopy Clustering process finished with error (non-zero return code)")

        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output file not created")
        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output cluster file not crated")

        with open(path.join(run_dir_name, "demo_profiles.out"), "r") as output_profiles_f:
            self.assertEqual(len(output_profiles_f.readlines()), 19, "Output number of clusters on demo matrix is different than 19")

    def test_run_excut(self):
        run_dir_name = "./test_run_{}/".format(str(datetime.now()).replace(" ", "_"))
        mkdir(run_dir_name)

        canopy_command = '../{} -n 4 -i ../demo_abundance_matrix.txt -o demo.out -c demo_profiles.out -p demo ' \
                     '--max_canopy_dist 0.1 --max_merge_dist 0.1 --excut_out_file_path ./excut_profiles.txt --excut_min_size 2 --excut_max_size 100000 --excut_threshold 0.4'.format(self.canopy_bin_path)

        with Popen(canopy_command, shell=True, stdout=PIPE, stderr=PIPE, bufsize=10, cwd=run_dir_name) as canopy_process:
            for line in canopy_process.stdout:
                info(">>\t" + line.decode('utf-8').rstrip())
            for line in canopy_process.stderr:
                warn(">>\t" + line.decode('utf-8').rstrip())

        self.assertEqual(canopy_process.returncode, 0, "Canopy Clustering process finished with error (non-zero return code)")

        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output file not created")
        self.assertTrue(path.isfile(path.join(run_dir_name, "demo.out")), "Output cluster file not crated")
        self.assertTrue(path.isfile(path.join(run_dir_name, "excut_profiles.txt")), "Excut file not crated")

        with open(path.join(run_dir_name, "demo_profiles.out"), "r") as output_profiles_f:
            self.assertEqual(len(output_profiles_f.readlines()), 19, "Output number of clusters on demo matrix is different than 19")


if __name__ == "__main__":
    unittest.main()



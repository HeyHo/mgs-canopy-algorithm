#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "../src/Log.hpp"
#include "../src/Point.hpp"

namespace bt = boost::unit_test;
namespace tt = boost::test_tools;

ProfileMeasureType profile_measure;

BOOST_AUTO_TEST_CASE( pearson_test )
{
    log_level = logDEBUG;
    profile_measure = MEAN;
    DistanceMeasureType distance_measure = PEARSON;

    Point point1("p1 1.5 2 5 6 3 6 13");
    point1.allocate_and_precompute_corr_data(distance_measure);
    Point point2("p2 1 4 5.4 3 9 0 8");
    point2.allocate_and_precompute_corr_data(distance_measure);

    BOOST_TEST( 0.3428 == 1 - get_distance_between_points(&point1, &point2), tt::tolerance(0.001));


    Point point3("p1 20 40 30 50 60");
    point3.allocate_and_precompute_corr_data(distance_measure);
    Point point4("p2 80 60 10 20 30");
    point4.allocate_and_precompute_corr_data(distance_measure);

    BOOST_TEST( -0.4881  == 1 - get_distance_between_points(&point3, &point4), tt::tolerance(0.001));
}

BOOST_AUTO_TEST_CASE( spearman_test )
{
    log_level = logDEBUG;
    profile_measure = MEAN;
    DistanceMeasureType distance_measure = SPEARMAN;

    Point point1("p1 1.5 2 5 6 3 6 13");
    point1.allocate_and_precompute_corr_data(distance_measure);
    Point point2("p2 1 4 5.4 3 9 0 8");
    point2.allocate_and_precompute_corr_data(distance_measure);

    BOOST_TEST( 0.1081 == 1 - get_distance_between_points(&point1, &point2), tt::tolerance(0.001));


    Point point3("p1 11 4 4 2 0 0 0 0 0 0 4 0 4 0 0 0 0 4 0 0 0 0 0");
    point3.allocate_and_precompute_corr_data(distance_measure);
    Point point4("p2 12 14 14 17 19 19 19 19 19 20 21 21 21 21 21 22 23 24 24 24 26 26 27");
 
    point4.allocate_and_precompute_corr_data(distance_measure);

    BOOST_TEST( -0.4233  == 1 - get_distance_between_points(&point3, &point4), tt::tolerance(0.001));
}